import numpy as np
import matplotlib.pyplot as plt

niz = np.random.randint(2, size = 100)
niz_gauss = np.random.randint(1, size = 100)

muski = []
zene = []

for i in range (0, 100):
    if niz[i] == 1:
        niz_gauss[i] = np.random.normal(180, 7, None)

    else:
        niz_gauss[i] = np.random.normal(167, 7, None)

for i in range (0, 100):
    if niz[i] == 1:
        muski.append(niz_gauss[i])
    else:
        zene.append(niz_gauss[i])

plt.hist([muski, zene], color = ['blue', 'red'])
plt.legend(["Muskarci", "Zene"])

n1 = np.average(muski)
n2 = np.average(zene)

plt.axvline(n1, color = 'blue', linestyle = ':', linewidth = 2)
plt.axvline(n2, color = 'red', linestyle = '--', linewidth = 2)

plt.show()
