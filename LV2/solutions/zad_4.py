import numpy as np
import matplotlib.pyplot as plt

br1 = 0
br6 = 0

niz = np.random.randint(1, 7, size = 100)

brojevi = range(min(niz), max(niz)+2)

plt.hist(niz, brojevi, color = "yellow")
plt.grid(linestyle='--')

for i in range (0,100):
    if niz[i] == 1:
        br1 += 1
    elif niz[i] == 6:
        br6 += 1
    else:
        False

print (br1, br6)
plt.show()