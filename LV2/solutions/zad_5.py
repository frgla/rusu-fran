import pandas as pd
import matplotlib.pyplot as plt

figure = plt.figure()
ax = figure.add_subplot(1,1,1)        

mtcars = pd.read_csv("mtcars.csv") 
#print mtcars
plt.scatter(mtcars.mpg, mtcars.hp)  
plt.xlabel("mpg")                   
plt.ylabel("hp")                   
plt.grid(linestyle='--')            


for i in range (0, len(mtcars.mpg)):        
    ax.annotate('%.2f' % mtcars.wt[i],     
                xy=(mtcars.mpg[i], mtcars.hp[i]), 
                xytext=(mtcars.mpg[i]+2, mtcars.hp[i]+20), 
                arrowprops=dict(arrowstyle="-", facecolor='black'),
                )

plt.show()