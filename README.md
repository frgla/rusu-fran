# Raspoznavanje uzoraka i strojno učenje

Ak.godina 2018./2019.


## Laboratorijske vježbe

Ovaj repozitorij sadrži potrebne datoteke za izradu svake laboratorijske vježbe (npr. LV1/resources). Rješenje svake laboratorijske vježbe treba spremiti u odgovarajući direktorij (npr. LV1/solutions).


## Podaci o studentu:

Ime i prezime: Fran Glavota
